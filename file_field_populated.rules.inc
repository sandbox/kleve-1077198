<?php


/**
 * Implementation of hook_rules_condition_info().
 */
function file_field_populated_rules_condition_info() {

  $info = array();
  $info['file_field_populated_check'] = array(
    'label' => t('File field is populated'),
    'arguments' => array(
      'node' => array('type' => 'node', 'label' => t('Content')),
    ),
    'eval input' => array('code'),
    'help' => t('The condition returns TRUE, if the selected field exists and is populated.'),
    'module' => 'CCK',
  );
  return $info;
}


/**
 * Checks if the selected file field exists and is populated.
 *
 * @param $node, The node we check the file field in.
 * @param $settings, The form settings.
 * @return True or False.
 */
function file_field_populated_check($node, $settings) {

  if (isset($node->$settings['file_field'])) {
    $file_field = $node->$settings['file_field'];
    if (!is_null($file_field[0])) {
      return TRUE;
    }
  }
  return FALSE;
}


/**
 * Adds a select list to the form with existing file fields.
 *
 * @param $settings, The form settings.
 * @param &$form, The form
 * @param &$form_state, The form state
 */
function file_field_populated_check_form($settings, &$form, &$form_state) {

  // Load CCK fields and make a list of available file fields.
  $fields = content_fields();
  $file_field_options = array();
  foreach ($fields as $field) {
    if ($field['type'] == 'filefield') {
      $file_field_options[$field['field_name']] = $field['widget']['label'];
    }
  }

  $form['settings']['file_field'] = array(
    '#type' => 'select',
    '#title' => t('File field'),
    '#options' => $file_field_options,
    '#default_value' => $settings['file_field'],
  );
}

/**
 * Sets the condition label.
 *
 * @param $settings, The form settings.
 * @return The new label.
 */
function file_field_populated_check_label($settings) {
  return t("File field (@field) exists and is not null", array('@field' => $settings['file_field']));
}